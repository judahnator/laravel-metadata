<?php

namespace judahnator\LaravelMetadata\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use judahnator\LaravelMetadata\HasMetadata;

class Apple extends Model
{
    use HasMetadata;

    protected $casts = [
        'metadata' => 'json'
    ];
    
    protected $fillable = [
        'name',
        'metadata'
    ];

    public $timestamps = false;
    
    public static function migrate(): void
    {
        Schema::dropIfExists('apples');
        Schema::create('apples', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->json('metadata')->default('{}');
        });
    }
}
