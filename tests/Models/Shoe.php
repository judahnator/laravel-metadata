<?php

namespace judahnator\LaravelMetadata\Tests\Models;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use judahnator\LaravelMetadata\MetadataModel;

class Shoe extends MetadataModel
{
    protected $fillable = [
        'owner',
        'details'
    ];

    protected $metadata = [
        'details' => 'object',
        'colors' => 'array'
    ];

    public $timestamps = false;

    public static function migrate(): void
    {
        Schema::dropIfExists('shoes');
        Schema::create('shoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('owner');
            $table->json('metadata')->default('{}');
        });
    }
}
