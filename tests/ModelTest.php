<?php

namespace judahnator\LaravelMetadata\Tests;

use judahnator\JsonManipulator\JsonBase;
use judahnator\LaravelMetadata\Tests\Models\Apple;
use judahnator\LaravelMetadata\Tests\Models\Shoe;
use Orchestra\Testbench\TestCase;

class ModelTest extends TestCase
{
    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function setUp()
    {
        parent::setUp();
    }

    public function testHasMetadataTrait(): void
    {
        Apple::migrate();

        // First, we need to test that data does not persist if not explicitly saved
        $apple = Apple::create(['name' => 'Gala']);
        $apple->meta->foo = "bar";
        $this->assertEquals('bar', $apple->meta->foo);
        $this->assertNull(Apple::first()->meta->foo);

        // Now we ensure that persisting the data works
        $apple->save();
        $this->assertEquals('bar', Apple::first()->meta->foo);
    }

    public function testMetadataModelClass(): void
    {
        Shoe::migrate();

        $shoe = Shoe::create(['owner' => 'judahnator']);

        // Set a value
        $shoe->details->foo = 'bar';
        $this->assertEquals('bar', $shoe->details->foo);

        // Sanity check to see if the "shortcut" is actually a shortcut
        $this->assertEquals($shoe->details->foo, $shoe->meta->details->foo);

        // Make sure we dont see the data persist yet
        $this->assertNull(Shoe::first()->details->foo);

        // Make sure data saves
        $shoe->save();
        $this->assertEquals('bar', Shoe::first()->details->foo);

        // test unsetting data
        unset($shoe->details->foo);
        $this->assertNull($shoe->details->foo);

        // Check to see if the variables are "portable"
        /** @var JsonBase $colors */
        $colors = $shoe->colors;
        $colors[] = 'blue';
        $colors[] = 'green';
        $this->assertEquals('blue', $shoe->colors[0]);
        $this->assertEquals('green', $shoe->colors[1]);
    }
}
