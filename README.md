Laravel Model Metadata Package
==============================

[![pipeline status](https://gitlab.com/judahnator/laravel-metadata/badges/master/pipeline.svg)](https://gitlab.com/judahnator/laravel-metadata/commits/master)

This package provides the ability to add "metadata" to your application models.


Model Setup
===========

First things first, you will need to add a JSON field to your models table.

```php
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

Schema::table('{your table}', function(Blueprint $table) {
    $table->json('metadata');
});
```

Next, for the most basic setup you just need to use the `HasMetadata` trait in your model.

```php
<?php

use Illuminate\Database\Eloquent\Model;
use judahnator\LaravelMetadata\HasMetadata;

class MyModel extends Model
{
    use HasMetadata;
}
```

And that's it. Now you can add metadata to your model to your hearts content, that wont persist to your database until you save your model.


Basic Usage
===========

You can access model metadata with a `meta` accessor. This accessor will always return a `judahnator\JsonManipulator\JsonBase` object. You may access its values like you would any other object.

When working with nested attributes, be that attributes nested in an array or object, you will work with either a `judahnator\JsonManipulator\JsonArray` or `judahnator\JsonManipulator\JsonObject` object depending on what is appropriate.

```php
<?php

$MyModel = MyModel::find(123);

// You can work with values directly
$MyModel->meta->foo = 'bar';

// You can also work with nested arrays and objects
$MyModel->meta->nested_array = ['foo', 'bar'];
$MyModel->meta->nested_object = (object)['keyed' => 'item'];

// Be aware that when retrieving arrays and objects, they are going to be represented with the JsonBase class.

/** @var \judahnator\JsonManipulator\JsonArray $nested_array */
$nested_array = $MyModel->meta->nested_array; 

/** @var \judahnator\JsonManipulator\JsonObject $nested_object */
$nested_object = $MyModel->meta->nested_object; 

// Because of how this library works internally, changing returned values changes the root value as well.
$nested_array[] = 'baz';
echo $MyModel->meta->nested_array[2]; // 'baz'

// Remember to save the model to make the metadata persist!
$MyModel->save();
```


More Advanced Usage
===================

In addition to the trait you can pull into the model, you can base your models off the `MetadataModel` class. This is completely optional, but allows you to "shortcut" some of your meta stores.

You must define your metadata stores in a protected `$metadata` array on your model. It is formatted with key=>value pairs, where the key is the store and the value is if the store is an array or object.

```php
<?php

use judahnator\LaravelMetadata\MetadataModel;

class MyModel extends MetadataModel
{
    protected $metadata = [
        'profile' => 'object',
        'hobbies' => 'array'
    ];
}

$MyModel = MyModel::find(123);
$MyModel->profile->youngest_grandchilds_name = 'Jimmy';
$MyModel->hobbies[] = 'hiking';
$MyModel->hobbies[] = 'coding';
```