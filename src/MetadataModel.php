<?php

namespace judahnator\LaravelMetadata;

use Illuminate\Database\Eloquent\Model;

abstract class MetadataModel extends Model
{
    use HasMetadata;

    public function __get($key)
    {
        if (array_key_exists($key, $this->metadataStores())) {
            if (!isset($this->getMetaAttribute()->{$key})) {
                switch ($this->metadataStores()[$key]) {

                    case 'array':
                        $this->getMetaAttribute()->{$key} = [];
                        break;

                    case 'object':
                    default:
                        $this->getMetaAttribute()->{$key} = (object)[];
                        break;

                }
            }

            return $this->getMetaAttribute()->{$key};
        }
        return parent::__get($key);
    }

    public function __set($key, $value)
    {
        if (array_key_exists($key, $this->metadataStores())) {
            $this->getMetaAttribute()->{$key} = $value;
        }
        parent::__set($key, $value);
    }
}
