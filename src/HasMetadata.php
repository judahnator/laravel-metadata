<?php

namespace judahnator\LaravelMetadata;

use judahnator\JsonManipulator\JsonBase;
use function judahnator\JsonManipulator\load_json;

trait HasMetadata
{
    public function metadataStores(): array
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return property_exists($this, 'metadata') ? $this->metadata : [];
    }

    public function getMetaDataColumn(): string
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return property_exists($this, 'metadata_column') ? $this->metadata_column : 'metadata';
    }

    public function getMetaAttribute(): JsonBase
    {
        if (!array_key_exists($this->getMetaDataColumn(), $this->attributes)) {
            $this->attributes[$this->getMetaDataColumn()] = '{}';
        }
        return load_json($this->attributes[$this->getMetaDataColumn()]);
    }
}
